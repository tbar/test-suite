package com.test.admin;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.util.SeleniumUtil;
import com.test.util.TestConstants;

import java.awt.Robot;
import java.awt.event.KeyEvent;

public class AppStoreTest extends SeleniumUtil {

	private final String addGroupTest_PROPERTIES_HTML_ID_FILENAME = "AppStore_HTML_ID.properties";
	private final String addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME = "AppStore_Assert_Values.properties";

	@BeforeMethod
	protected void bizappcenterTest_suite_pre_function() throws Exception {
		bizappcenterTest_properties_Assert_Values_file = addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME;
		bizappcenterTest_properties_HTML_ID_file = addGroupTest_PROPERTIES_HTML_ID_FILENAME;
		bizappcenterTest_suite_setup();
	}

	// Test_case for create New User in bizappcenter
	@Test(priority = TestConstants.INT_1)
	public void appStore_createNewUser_test() throws Exception {

		bizappLogin(browser);

		// bizapp_click_on_User_Tab
		WebElement bizapp_user_tab = doc_get("bizappcenter_user_tab_xpath",
				browser);
		Assert.assertTrue("bizappcenter_user_tab_display_assert_false",
				bizapp_user_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizapp_user_tab.click();
		browser_wait(TestConstants.LONG_1000);

		// bizapp_click_on_AddUser_Tab
		WebElement bizapp_addUser_tab = doc_get("bizappcenter_addUser_xpath",
				browser);
		Assert.assertTrue("bizappcenter_addUser_tab_display_assert_false",
				bizapp_addUser_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizapp_addUser_tab.click();
		browser_wait(TestConstants.LONG_4000);

		// addUser_fullName_textfield_enter_name
		WebElement addUser_full_name_textfield = doc_get(
				"addUser_full_name_textfield_xptah", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_full_name_textfield_display_assert_false",
				addUser_full_name_textfield.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_full_name_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_full_name"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_emailTextField_enter_email
		WebElement addUser_email_textfield = doc_get(
				"addUser_email_textfield_xptah", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_email_textfield_display_assert_false",
				addUser_email_textfield.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_name"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_passwordTextfield_enter_password
		WebElement addUser_password_textfield = doc_get(
				"addUser_password_textfield_xptah", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_password_textfield_display_assert_false",
				addUser_password_textfield.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_password"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_confermPassTxtfield_enter_password
		WebElement addUser_conferm_password_textfield = doc_get(
				"addUser_conferm_password_textfield_xptah", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_conferm_password_textfield_display_assert_false",
				addUser_conferm_password_textfield.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_conferm_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_confermpassword"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_group_select_group
		List<WebElement> addUser_groups_textarea = doc_list_get(
				"addUser_groups_textarea_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_groups_textarea_display_assert_false",
				addUser_groups_textarea.size() > 0);
		addUser_groups_textarea.get(1).click();
		browser_wait(TestConstants.LONG_1000);
		addUser_groups_textarea.get(0).click();
		browser_wait(TestConstants.LONG_2000);

		// addUser_notify_user_checkbox_select
		WebElement addUser_notify_user_chekbox = doc_get(
				"addUser_notify_user_chekbox_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_notify_user_chekbox_display_assert_false",
				addUser_notify_user_chekbox.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_notify_user_chekbox.click();
		browser_wait(TestConstants.LONG_2000);

		// addUser_create_user_button_click
		WebElement addUser_create_user_button = doc_get(
				"addUser_create_user_button_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_create_user_button_display_assert_false",
				addUser_create_user_button.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_create_user_button.click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_Logout_button
		WebElement bizappcenter_logout_button = doc_get(
				"bizappcenter_logout_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_logout_button.click();
		browser_wait(TestConstants.LONG_2000);
	}

	// Test_case for create New User in bizappcenter
	@Test(priority = TestConstants.INT_2)
	public void appStore_newUser_agreement_test() throws Exception {

		browser_wait(TestConstants.LONG_2000);
		// bizappcenter_login_username
		WebElement bizappcenter_username = doc_get(
				"appStore_login_userName_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_username
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_name"));

		// bizappcenter_login_password
		browser_wait(TestConstants.LONG_2000);
		WebElement bizappcenter_password = doc_get(
				"appStore_login_passworg_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_password
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_password"));

		// bizappcenter_login_button_click
		browser_wait(TestConstants.LONG_2000);
		WebElement bizappcenter_login_button = doc_get(
				"appStore_login_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_login_button.click();
		browser_wait(TestConstants.LONG_7000);

		// appStore_conferm_agreement_by_checking_checkbox
		WebElement newUser_agreement_checkbox = doc_get(
				"newUser_agreement_checkbox_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		newUser_agreement_checkbox.click();

		// WebElement appStore_agreement_message = doc_get(
		// "agreement_message_xpath", browser);
		// Assert.assertTrue(
		// "bizappcenter_appStore_agreement_message_display_assert_false",
		// appStore_agreement_message
		// .getText()
		// .contains(
		// bizappcenterTest_properties_assert_values_get("newUser_agreement_text")));

		// Click_on_submit_button_to_accept_agreement
		browser_wait(TestConstants.LONG_2000);
		WebElement newUser_agreement_submitButton = doc_get(
				"newUser_agreement_submitButton_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		newUser_agreement_submitButton.click();
		browser_wait(TestConstants.LONG_2000);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);

		WebElement appStore_clickHereLink = doc_get(
				"appStore_click_here_link_xpath", browser);

		Actions action = new Actions(browser);
		action.click(appStore_clickHereLink).build().perform();
		 browser_wait(TestConstants.LONG_2000);
		
		robot.keyPress(KeyEvent.VK_TAB);
		robot.delay(1000);
		robot.keyPress(KeyEvent.VK_SPACE);
		robot.keyRelease(KeyEvent.VK_SPACE);
		robot.delay(1000);
		robot.keyPress(KeyEvent.VK_TAB);
		robot.delay(1000);
		robot.keyPress(KeyEvent.VK_ENTER);

		browser_wait(TestConstants.LONG_30000);
		WebElement newUser_associateButton = doc_get(
				"newUser_associate_with_system_button_xpath", browser);
		newUser_associateButton.click();
		browser_wait(TestConstants.LONG_3000);

		browser_wait(TestConstants.LONG_10000);
		browser_wait(TestConstants.LONG_5000);

		// List<WebElement> appStore_status_listOfApp = doc_list_get(
		// "appStore_list_of_apps_xpath", browser);
		// Map<String, String> listOfApps = new HashMap<String, String>();
		// listOfApps.put(key, value);

		WebElement installApp_button = doc_get(
				"appStore_installApp_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		action = new Actions(browser);
		action.click(installApp_button).build().perform();
		// browser_wait(TestConstants.LONG_30000);
		// browser_wait(TestConstants.LONG_30000);
		// browser_wait(TestConstants.LONG_30000);

	}

}
