package com.test.admin;

import junit.framework.Assert;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.util.SeleniumUtil;
import com.test.util.TestConstants;

public class SocialTest extends SeleniumUtil {

	private final String addGroupTest_PROPERTIES_HTML_ID_FILENAME = "Social_HTML_ID.properties";
	private final String addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME = "Social_Assert_Values.properties";

	@BeforeMethod
	protected void bizappcenterTest_suite_pre_function() throws Exception {
		bizappcenterTest_properties_Assert_Values_file = addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME;
		bizappcenterTest_properties_HTML_ID_file = addGroupTest_PROPERTIES_HTML_ID_FILENAME;
		bizappcenterTest_suite_setup();
	}

	// Test_case for ManageGroup in bizappcenter
	@Test(priority = TestConstants.INT_1)
	public void bizappcenter_manageGroup_test() throws Exception {

		// Login_for_Test_User
		bizappLogin(browser);

		// Click_on_Social_Tab
		WebElement bizapp_social_tab = doc_get("bizAppCenter_social_tab_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_social_tab.click();
		browser_wait(TestConstants.LONG_2000);

		// bizapp_Social_click_on_addAnnouncement_Tab
		WebElement socialTab_addAnnouncement_tab = doc_get(
				"socialTab_addAnnouncement_tab_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_social_addAnnouncement_tab_display_assert_false",
				socialTab_addAnnouncement_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		socialTab_addAnnouncement_tab.click();
		browser_wait(TestConstants.LONG_3000);

		WebElement addAnnouncement_title_textField = doc_get(
				"addAnnouncement_title_textField_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_title_textField
				.sendKeys(bizappcenterTest_properties_assert_values_get("addAnnouncement_title_text_sendkey"));
		browser_wait(TestConstants.LONG_3000);

		WebElement addAnnouncement_body_textArea = doc_get(
				"addAnnouncement_body_textArea_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_body_textArea
				.sendKeys(bizappcenterTest_properties_assert_values_get("addAnnouncement_body_text_sendkey"));
		browser_wait(TestConstants.LONG_3000);
		
		WebElement addAnnouncement_preview_button = doc_get(
				"addAnnouncement_preview_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_preview_button.click();
		browser_wait(TestConstants.LONG_4000);
		
		WebElement addAnnouncement_submit_button = doc_get(
				"addAnnouncement_submit_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_submit_button.click();
		browser_wait(TestConstants.LONG_4000);


	}

}
