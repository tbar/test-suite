package com.test.admin;

import org.openqa.selenium.WebElement;
import junit.framework.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.util.SeleniumUtil;
import com.test.util.TestConstants;

public class BizappLoginTest extends SeleniumUtil {

	private final String bizappLoginTest_PROPERTIES_HTML_ID_FILENAME = "Bizapp_Login_HTML_ID1.properties";
	private final String bizappLoginTest_PROPERTIES_ASSERT_VALUES_FILENAME = "Bizapp_Login_Assert_Values.properties";

	@BeforeMethod
	protected void bizappcenterTest_suite_pre_function() throws Exception {
		try {
			bizappcenterTest_properties_Assert_Values_file = bizappLoginTest_PROPERTIES_ASSERT_VALUES_FILENAME;
			bizappcenterTest_properties_HTML_ID_file = bizappLoginTest_PROPERTIES_HTML_ID_FILENAME;
			bizappcenterTest_suite_setup();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	// Test_case for bizappcenter_login
	@Test(priority = TestConstants.INT_1)
	public void bizappcenter_login_test() throws Exception {

		browser_wait(TestConstants.LONG_2000);
		// bizappcenter_login_username
		WebElement bizappcenter_username = doc_get(
				"bizappcenter_username_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		Assert.assertTrue("bizappcenter_login_username_display_assert_false",
				bizappcenter_username.isDisplayed());
		bizappcenter_username
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_login_username_sendkeys"));

		// bizappcenter_login_password
		browser_wait(TestConstants.LONG_2000);
		WebElement bizappcenter_password = doc_get(
				"bizappcenter_password_xpath", browser);
		Assert.assertTrue("bizappcenter_login_password_display_assert_false",
				bizappcenter_password.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_password
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_login_password_sendkeys"));

		// bizappcenter_login_button_click
		browser_wait(TestConstants.LONG_2000);
		WebElement bizappcenter_login_button = doc_get(
				"bizappcenter_login_button_xpath", browser);
		Assert.assertTrue("bizappcenter_login_button_display_assert_false",
				bizappcenter_login_button.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_login_button.click();
		browser_wait(TestConstants.LONG_7000);

	}

}
