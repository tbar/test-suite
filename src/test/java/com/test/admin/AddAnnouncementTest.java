package com.test.admin;

import junit.framework.Assert;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.util.SeleniumUtil;
import com.test.util.TestConstants;

public class AddAnnouncementTest extends SeleniumUtil{
	
	private final String addGroupTest_PROPERTIES_HTML_ID_FILENAME = "Social_HTML_ID.properties";
	private final String addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME = "Social_Assert_Values.properties";

	@BeforeMethod
	protected void bizappcenterTest_suite_pre_function() throws Exception {
		bizappcenterTest_properties_Assert_Values_file = addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME;
		bizappcenterTest_properties_HTML_ID_file = addGroupTest_PROPERTIES_HTML_ID_FILENAME;
		bizappcenterTest_suite_setup();
	}

	// Test_case for Social in bizappcenter
	@Test(priority = TestConstants.INT_1)
	public void bizappcenter_create_social_addAnnouncement_test()
			throws Exception {

		// Login_for_Test_User
		bizappLogin(browser, "AdminClassTest_Assert_Values.properties",
				"Bizapp_Login_HTML_ID1.properties");

		// Click_on_Social_Tab
		WebElement bizapp_social_tab = doc_get("bizAppCenter_social_tab_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_social_tab.click();
		browser_wait(TestConstants.LONG_2000);

		// bizapp_Social_click_on_addAnnouncement_Tab
		WebElement socialTab_addAnnouncement_tab = doc_get(
				"socialTab_addAnnouncement_tab_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_social_addAnnouncement_tab_display_assert_false",
				socialTab_addAnnouncement_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		socialTab_addAnnouncement_tab.click();
		browser_wait(TestConstants.LONG_3000);

		WebElement addAnnouncement_title_textField = doc_get(
				"addAnnouncement_title_textField_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_title_textField
				.sendKeys(bizappcenterTest_properties_assert_values_get("addAnnouncement_title_text_sendkey"));
		browser_wait(TestConstants.LONG_3000);

		WebElement addAnnouncement_body_textArea = doc_get(
				"addAnnouncement_body_textArea_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_body_textArea
				.sendKeys(bizappcenterTest_properties_assert_values_get("addAnnouncement_body_text_sendkey"));
		browser_wait(TestConstants.LONG_3000);

		WebElement addAnnouncement_preview_button = doc_get(
				"addAnnouncement_preview_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_preview_button.click();
		browser_wait(TestConstants.LONG_4000);

		WebElement addAnnouncement_submit_button = doc_get(
				"addAnnouncement_submit_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_submit_button.click();
		browser_wait(TestConstants.LONG_4000);
	}

	// Test_case for test announcement in bizappcenter
	@Test(priority = TestConstants.INT_2)
	public void bizappcenter_testing_of_addAnnouncement_test() throws Exception {

		// User login
		bizappLogin(browser, "Social_Assert_Values.properties",
				"Bizapp_Login_HTML_ID1.properties");

		// appStore_conferm_agreement_by_checking_checkbox
		WebElement newUser_agreement_checkbox = doc_get(
				"newUser_agreement_checkbox_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		newUser_agreement_checkbox.click();

		// Click_on_submit_button_to_accept_agreement
		browser_wait(TestConstants.LONG_2000);
		WebElement newUser_agreement_submitButton = doc_get(
				"newUser_agreement_submitButton_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		newUser_agreement_submitButton.click();
		browser_wait(TestConstants.LONG_2000);

		WebElement newUser_announcement_link = doc_get(
				"endUser_announcement_link_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		newUser_announcement_link.click();
		browser_wait(TestConstants.LONG_4000);
	}

	// Test_case for test announcement in bizappcenter
	@Test(priority = TestConstants.INT_3)
	public void bizappcenter_edit_addAnnouncement_test() throws Exception {

		// Login_for_Test_User
		bizappLogin(browser, "AdminClassTest_Assert_Values.properties",
				"Bizapp_Login_HTML_ID1.properties");

		 // appStore_conferm_agreement_by_checking_checkbox
		 WebElement newUser_agreement_checkbox = doc_get(
		 "newUser_agreement_checkbox_xpath", browser);
		 browser_wait(TestConstants.LONG_1000);
		 newUser_agreement_checkbox.click();
		
		 // Click_on_submit_button_to_accept_agreement
		 browser_wait(TestConstants.LONG_2000);
		 WebElement newUser_agreement_submitButton = doc_get(
		 "newUser_agreement_submitButton_xpath", browser);
		 browser_wait(TestConstants.LONG_1000);
		 newUser_agreement_submitButton.click();
		 browser_wait(TestConstants.LONG_2000);

		// click-on_switchLink
		WebElement bizAppCenter_switch_link = doc_get(
				"bizAppCenter_switch_link_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizAppCenter_switch_link.click();
		browser_wait(TestConstants.LONG_4000);

		// click_on_announcementLink
		WebElement newUser_announcement_link = doc_get(
				"endUser_announcement_link_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		newUser_announcement_link.click();
		browser_wait(TestConstants.LONG_4000);

		// click_on_announcement_editTab
		WebElement announcement_edit_tab = doc_get(
				"addAnnouncement_edit_tab_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		announcement_edit_tab.click();
		browser_wait(TestConstants.LONG_4000);
		
		// edit_title_of_addAnnouncement
		WebElement addAnnouncement_title_textField = doc_get(
				"addAnnouncement_title_textField_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_title_textField.clear();
		browser_wait(TestConstants.LONG_2000);
		addAnnouncement_title_textField
				.sendKeys(bizappcenterTest_properties_assert_values_get("edited_addAnnouncement_title_text_sendkey"));
		browser_wait(TestConstants.LONG_3000);

		WebElement addAnnouncement_body_textArea = doc_get(
				"addAnnouncement_body_textArea_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_body_textArea
				.sendKeys(bizappcenterTest_properties_assert_values_get("edited_addAnnouncement_body_text_sendkey"));
		browser_wait(TestConstants.LONG_3000);
		
		WebElement addAnnouncement_preview_button = doc_get(
				"addAnnouncement_preview_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_preview_button.click();
		browser_wait(TestConstants.LONG_4000);
		
		WebElement addAnnouncement_viewChanges_button = doc_get(
				"addAnnouncement_viewChanges_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_viewChanges_button.click();
		browser_wait(TestConstants.LONG_5000);
		
		WebElement addAnnouncement_submit_button = doc_get(
				"addAnnouncement_submit_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addAnnouncement_submit_button.click();
		browser_wait(TestConstants.LONG_4000);
	}

	// Test_case for recheck updated announcement in bizappcenter
		@Test(priority = TestConstants.INT_4)
		public void bizappcenter_recheck_of_updated_addAnnouncement_test() throws Exception {
	
			// User login
			bizappLogin(browser, "Social_Assert_Values.properties",
					"Bizapp_Login_HTML_ID1.properties");
	
			WebElement newUser_announcement_link = doc_get(
					"endUser_announcement_link_xpath", browser);
			browser_wait(TestConstants.LONG_1000);
			newUser_announcement_link.click();
			browser_wait(TestConstants.LONG_4000);
		}
		
		// Test_case for delete announcement in bizappcenter
		@Test(priority = TestConstants.INT_5)
		public void delete_addAnnouncement_test() throws Exception {

			// Login_for_Test_User
			bizappLogin(browser, "AdminClassTest_Assert_Values.properties",
					"Bizapp_Login_HTML_ID1.properties");

			// click-on_switchLink
			WebElement bizAppCenter_switch_link = doc_get(
					"bizAppCenter_switch_link_xpath", browser);
			browser_wait(TestConstants.LONG_1000);
			bizAppCenter_switch_link.click();
			browser_wait(TestConstants.LONG_4000);

			// click_on_announcementLink
			WebElement newUser_announcement_link = doc_get(
					"endUser_announcement_link_xpath", browser);
			browser_wait(TestConstants.LONG_1000);
			newUser_announcement_link.click();
			browser_wait(TestConstants.LONG_4000);

			// click_on_announcement_editTab
			WebElement announcement_edit_tab = doc_get(
					"addAnnouncement_edit_tab_xpath", browser);
			browser_wait(TestConstants.LONG_1000);
			announcement_edit_tab.click();
			browser_wait(TestConstants.LONG_4000);
			
			// click_on_delete_of_addAnnouncement
			WebElement addAnnouncement_delete_button = doc_get(
					"addAnnouncement_delete_button_xpath", browser);
			browser_wait(TestConstants.LONG_1000);
			addAnnouncement_delete_button.click();
			browser_wait(TestConstants.LONG_4000);
			
			// click_on_confermDelete_of_addAnnouncement
			WebElement addAnnouncement_conferm_delete_button = doc_get(
					"conferm_delete_announcement_button_xpath", browser);
			browser_wait(TestConstants.LONG_1000);
			addAnnouncement_conferm_delete_button.click();
			browser_wait(TestConstants.LONG_4000);
			
			bizAppCenter_switch_link = doc_get(
					"bizAppCenter_switch_link_xpath", browser);
			browser_wait(TestConstants.LONG_1000);
			bizAppCenter_switch_link.click();
			browser_wait(TestConstants.LONG_4000);
			
			bizAppCenter_switch_link = doc_get(
					"bizAppCenter_switch_link_xpath", browser);
			browser_wait(TestConstants.LONG_1000);
			bizAppCenter_switch_link.click();
			browser_wait(TestConstants.LONG_4000);
		}

}
