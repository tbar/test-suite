package com.test.admin;

import java.util.List;

import org.openqa.selenium.WebElement;
import junit.framework.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.util.SeleniumUtil;
import com.test.util.TestConstants;

//Test for Create Login application for Admin user.
@Test(threadPoolSize = 1, groups = { "bizappcenterTest_suite" })
public class CreateUserTest extends SeleniumUtil {

	private final String addUserTest_PROPERTIES_HTML_ID_FILENAME = "AddUser_HTML_ID.properties";
	private final String addUserTest_PROPERTIES_ASSERT_VALUES_FILENAME = "AddUser_Assert_Values.properties";

	@BeforeMethod
	protected void bizappcenterTest_suite_pre_function() throws Exception {
		bizappcenterTest_properties_Assert_Values_file = addUserTest_PROPERTIES_ASSERT_VALUES_FILENAME;
		bizappcenterTest_properties_HTML_ID_file = addUserTest_PROPERTIES_HTML_ID_FILENAME;
		bizappcenterTest_suite_setup();
	}

	// Test_case for create_User in bizappcenter
	@Test(priority = TestConstants.INT_1)
	public void bizappcenter_createUser_test() throws Exception {

		bizappLogin(browser,"Bizapp_Login_Assert_Values.properties","Bizapp_Login_HTML_ID1.properties");
		// bizapp_click_on_user_Tab
		WebElement bizapp_user_tab = doc_get("bizappcenter_user_tab_xpath",
				browser);
		Assert.assertTrue("bizappcenter_user_tab_display_assert_false",
				bizapp_user_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizapp_user_tab.click();
		browser_wait(TestConstants.LONG_1000);

		// bizapp_click_on_addUser
		WebElement bizapp_addUser_tab = doc_get("bizappcenter_addUser_xpath",
				browser);
		Assert.assertTrue("bizappcenter_addUser_tab_display_assert_false",
				bizapp_addUser_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizapp_addUser_tab.click();
		browser_wait(TestConstants.LONG_4000);

		// addUser_fullName_textfield_enter_name
		WebElement addUser_full_name_textfield = doc_get(
				"addUser_full_name_textfield_xptah", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_full_name_textfield_display_assert_false",
				addUser_full_name_textfield.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_full_name_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_full_name"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_email_textfield_enter_Invalid_email
		WebElement addUser_email_textfield = doc_get(
				"addUser_email_textfield_xptah", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_email_textfield_display_assert_false",
				addUser_email_textfield.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_wrong_email"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_password_textfield_enter_password
		WebElement addUser_password_textfield = doc_get(
				"addUser_password_textfield_xptah", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_password_textfield_display_assert_false",
				addUser_password_textfield.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_password"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_confermPass_textfield_enter_wrong_password
		WebElement addUser_conferm_password_textfield = doc_get(
				"addUser_conferm_password_textfield_xptah", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_conferm_password_textfield_display_assert_false",
				addUser_conferm_password_textfield.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_conferm_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_confermpassword1"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_group_select_group
		List<WebElement> addUser_groups_textarea = doc_list_get(
				"addUser_groups_textarea_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_groups_textarea_display_assert_false",
				addUser_groups_textarea.size() > 0);
		addUser_groups_textarea.get(2).click();
		browser_wait(TestConstants.LONG_1000);
		addUser_groups_textarea.get(0).click();
		browser_wait(TestConstants.LONG_2000);

		// addUser_notify_user_checkbox_select
		WebElement addUser_notify_user_chekbox = doc_get(
				"addUser_notify_user_chekbox_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_notify_user_chekbox_display_assert_false",
				addUser_notify_user_chekbox.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_notify_user_chekbox.click();
		browser_wait(TestConstants.LONG_2000);

		// addUser_create_user_button_click
		WebElement addUser_create_user_button = doc_get(
				"addUser_create_user_button_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_create_user_button_display_assert_false",
				addUser_create_user_button.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_create_user_button.click();
		browser_wait(TestConstants.LONG_2000);

		// wrong_mail_id_error_message_display
		WebElement addUser_error_message = doc_get(
				"addUser_error_message_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_wrong_password_error_message_display_assert_false",
				addUser_error_message.getText().contains(
						"The specified passwords do not match."));
		Assert.assertTrue(
				"bizappcenter_addUser_wrong_mailID_error_message_display_assert_false",
				addUser_error_message.getText().contains(
						"The e-mail address abcdefgh.1234 is not valid."));

		// addUser_email_textfield_enter_rightEmail
		addUser_email_textfield = doc_get("addUser_email_textfield_xptah",
				browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_name"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_password_textfield_enter_password
		addUser_password_textfield = doc_get(
				"addUser_password_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_password"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_conferm_pass_textfield_enter_right_password
		addUser_conferm_password_textfield = doc_get(
				"addUser_conferm_password_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_conferm_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_confermpassword2"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_create_user_button_click
		addUser_create_user_button = doc_get(
				"addUser_create_user_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_create_user_button.click();
		browser_wait(TestConstants.LONG_2000);

		addUser_error_message = doc_get("addUser_error_message_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_existing_user_error_message_display_assert_false",
				addUser_error_message
						.getText()
						.contains(
								"The e-mail address abcdefgh.12345@abc123 is already registered."));

		// addUser_fullName_textfield_enter_name
		addUser_full_name_textfield = doc_get(
				"addUser_full_name_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_full_name_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addUser_full_name_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_full_name2"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_email_textfield_enter_rightEmail
		addUser_email_textfield = doc_get("addUser_email_textfield_xptah",
				browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_name2"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_password_textfield_enter_password
		addUser_password_textfield = doc_get(
				"addUser_password_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_password"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_conferm_pass_textfield_enter_right_password
		addUser_conferm_password_textfield = doc_get(
				"addUser_conferm_password_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_conferm_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_confermpassword2"));
		browser_wait(TestConstants.LONG_2000);

		WebElement addUser_roles_checkbox = doc_get(
				"addUser_roles_checkbox_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addUser_roles_checkbox_display_assert_false",
				addUser_roles_checkbox.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addUser_roles_checkbox.click();
		browser_wait(TestConstants.LONG_2000);

		// addUser_create_user_button_click
		addUser_create_user_button = doc_get(
				"addUser_create_user_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_create_user_button.click();
		browser_wait(TestConstants.LONG_4000);

		/************************************************************************************/

		// addUser_fullName_textfield_enter_name
		addUser_full_name_textfield = doc_get(
				"addUser_full_name_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_full_name_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addUser_full_name_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_full_testingname1"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_email_textfield_enter_rightEmail
		addUser_email_textfield = doc_get("addUser_email_textfield_xptah",
				browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_testigname1"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_password_textfield_enter_password
		addUser_password_textfield = doc_get(
				"addUser_password_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_testingname_pass"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_conferm_pass_textfield_enter_right_password
		addUser_conferm_password_textfield = doc_get(
				"addUser_conferm_password_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_conferm_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_testingname_pass"));
		browser_wait(TestConstants.LONG_2000);

		addUser_groups_textarea = doc_list_get("addUser_groups_textarea_xpath",
				browser);
		addUser_groups_textarea.get(0).click();
		browser_wait(TestConstants.LONG_1000);
		addUser_groups_textarea.get(2).click();
		browser_wait(TestConstants.LONG_2000);

		// addUser_create_user_button_click
		addUser_create_user_button = doc_get(
				"addUser_create_user_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_create_user_button.click();
		browser_wait(TestConstants.LONG_4000);

		/***********************************************************************************/

		// addUser_fullName_textfield_enter_name
		addUser_full_name_textfield = doc_get(
				"addUser_full_name_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_full_name_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addUser_full_name_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_full_testingname2"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_email_textfield_enter_rightEmail
		addUser_email_textfield = doc_get("addUser_email_textfield_xptah",
				browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addUser_email_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_testigname2"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_password_textfield_enter_password
		addUser_password_textfield = doc_get(
				"addUser_password_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_testingname_pass"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_conferm_pass_textfield_enter_right_password
		addUser_conferm_password_textfield = doc_get(
				"addUser_conferm_password_textfield_xptah", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_conferm_password_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addUser_email_testingname_pass"));
		browser_wait(TestConstants.LONG_2000);

		// addUser_create_user_button_click
		addUser_create_user_button = doc_get(
				"addUser_create_user_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addUser_create_user_button.click();
		browser_wait(TestConstants.LONG_2000);

		/***********************************************************************************/

		// Click_on_Logout_button
		WebElement bizappcenter_logout_button = doc_get(
				"bizappcenter_logout_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_logout_button.click();
		browser_wait(TestConstants.LONG_2000);

	}

}
