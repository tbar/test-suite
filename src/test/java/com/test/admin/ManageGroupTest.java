package com.test.admin;

import java.util.List;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.util.SeleniumUtil;
import com.test.util.TestConstants;

public class ManageGroupTest extends SeleniumUtil {

	private final String addGroupTest_PROPERTIES_HTML_ID_FILENAME = "ManageGroup_HTML_ID.properties";
	private final String addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME = "ManageGroup_Assert_Values.properties";

	@BeforeMethod
	protected void bizappcenterTest_suite_pre_function() throws Exception {
		bizappcenterTest_properties_Assert_Values_file = addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME;
		bizappcenterTest_properties_HTML_ID_file = addGroupTest_PROPERTIES_HTML_ID_FILENAME;
		bizappcenterTest_suite_setup();
	}

	// Test_case for ManageGroup in bizappcenter
	@Test(priority = TestConstants.INT_1)
	public void bizappcenter_manageGroup_test() throws Exception {

		// Login_for_Test_User
		bizappLogin(browser,"AdminClassTest_Assert_Values.properties","Bizapp_Login_HTML_ID1.properties");

		// Click_on_Users_Tab
		WebElement bizapp_user_tab = doc_get("bizappcenter_user_tab_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_user_tab.click();
		browser_wait(TestConstants.LONG_1000);

		// bizapp_Users_click_on_ManageUser_Tab
		WebElement bizapp_manageGroup_tab = doc_get(
				"bizappcenter_manageGroup_tab_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_user_manageGroup_tab_display_assert_false",
				bizapp_manageGroup_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizapp_manageGroup_tab.click();
		browser_wait(TestConstants.LONG_3000);

		// // Select_any_group_to_move
		// WebElement manageGroup_groupName_movingElement = doc_get(
		// "manageGroup_groupName_movingElement_xpath", browser);
		// browser_wait(TestConstants.LONG_1000);
		//
		// // Calculate_X_position_of_selected_group
		// int oldPos =
		// manageGroup_groupName_movingElement.getLocation().getX();
		//
		// // Move_the_selected_element
		// MoveElementForward(manageGroup_groupName_movingElement,
		// manageGroup_groupName_movingElement);
		//
		// // Calculate_X_position_after_moving
		// int newPos =
		// manageGroup_groupName_movingElement.getLocation().getX();
		// if (oldPos == newPos) {
		// MoveElementBackward(manageGroup_groupName_movingElement,
		// manageGroup_groupName_movingElement);
		// }
		// browser_wait(TestConstants.LONG_3000);

		// Click_on_edit_link_of_selected_group
		WebElement manageGroup_groupName_edit = doc_get(
				"manageGroup_groupName_edit_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageGroup_groupName_edit.click();
		browser_wait(TestConstants.LONG_3000);

		// Select_Parent_of_selected_child_group
		List<WebElement> manageGroup_editUser_selectParent_textArea = doc_list_get(
				"manageGroup_editUser_selectParent_textArea_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageGroup_editUser_selectParent_textArea_display_assert_false",
				manageGroup_editUser_selectParent_textArea.size() > 0);
		manageGroup_editUser_selectParent_textArea.get(
				(manageGroup_editUser_selectParent_textArea.size()) - 4)
				.click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_updateGroup_button
		WebElement manageGroup_editUser_updateGroup_button = doc_get(
				"manageGroup_editUser_updateGroup_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageGroup_editUser_updateGroup_button.click();
		browser_wait(TestConstants.LONG_3000);

		// Click_on_saveButton_to_save_changes
		WebElement manageGroup_groupName_saveButton = doc_get(
				"manageGroup_groupName_saveButton_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageGroup_groupName_saveButton.click();
		browser_wait(TestConstants.LONG_3000);

		// Click_on_Manage_Tab
		WebElement bizappcenter_manage_tab = doc_get(
				"bizappcenter_manage_tab_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_manage_tab.click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_appDistribution_Tab_to_see_changes
		WebElement bizappcenter_manageTab_appDistribution_tab = doc_get(
				"bizappcenter_manageTab_appDistribution_tab_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_manageTab_appDistribution_tab.click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_Active_userList
		WebElement appDistribution_active_usersList = doc_get(
				"appDistribution_active_usersList_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		appDistribution_active_usersList.click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_subLists_of_ActiveUserList
		List<WebElement> bizappcenter_manage_activeUser = doc_list_get(
				"bizappcenter_manage_activeUser_xpath", browser);
		bizappcenter_manage_activeUser.get(0).findElement(By.tagName("img"))
				.click();
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_manage_activeUser.get(1).findElement(By.tagName("img"))
				.click();
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_manage_activeUser.get(2).findElement(By.tagName("img"))
				.click();
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_manage_activeUser.get(3).findElement(By.tagName("img"))
				.click();
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_manage_activeUser.get(4).findElement(By.tagName("img"))
				.click();
		browser_wait(TestConstants.LONG_2000);

	}

}
