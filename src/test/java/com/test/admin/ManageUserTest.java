package com.test.admin;

import java.util.List;

import junit.framework.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.util.SeleniumUtil;
import com.test.util.TestConstants;

public class ManageUserTest extends SeleniumUtil {
	private final String addGroupTest_PROPERTIES_HTML_ID_FILENAME = "ManageUser_HTML_ID.properties";
	private final String addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME = "ManageUser_Assert_Values.properties";

	@BeforeMethod
	protected void bizappcenterTest_suite_pre_function() throws Exception {
		bizappcenterTest_properties_Assert_Values_file = addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME;
		bizappcenterTest_properties_HTML_ID_file = addGroupTest_PROPERTIES_HTML_ID_FILENAME;
		bizappcenterTest_suite_setup();
	}

	// Test_case for ManageUser in bizappcenter
	@Test(priority = TestConstants.INT_1)
	public void bizappcenter_manageUser_test() throws Exception {

		bizappLogin(browser,"AdminClassTest_Assert_Values.properties","Bizapp_Login_HTML_ID1.properties");
		// bizapp_click_on_User_Tab
		WebElement bizapp_user_tab = doc_get("bizappcenter_user_tab_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_user_tab.click();
		browser_wait(TestConstants.LONG_1000);

		// bizapp_UserTab_click_on_manageUser_Tab
		WebElement bizapp_manageUser_tab = doc_get(
				"bizappcenter_user_manageUserTab_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_user_manageUser_tab_display_assert_false",
				bizapp_manageUser_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizapp_manageUser_tab.click();
		browser_wait(TestConstants.LONG_3000);

		// manageUser_Click_on_group_dropDownList
		WebElement manageUser_group_dropDownList = doc_get(
				"manageUser_group_dropDownList_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_dropDownList_display_assert_false",
				manageUser_group_dropDownList.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		manageUser_group_dropDownList.click();
		browser_wait(TestConstants.LONG_1000);

		// Select_anyGroup_from_dropDownList
		List<WebElement> manageUser_group_dropDownList_elements = doc_list_get(
				"manageUser_group_dropDownList_elements_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_dropDownList_elements_display_assert_false",
				manageUser_group_dropDownList_elements.size() > 0);
		browser_wait(TestConstants.LONG_1000);
		manageUser_group_dropDownList_elements.get(2).click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_filterButton_to_Filter_selectedGroup
		WebElement manageUser_group_filterButton = doc_get(
				"manageUser_refineFilter_button_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_group_filterButton_display_assert_false",
				manageUser_group_filterButton.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		manageUser_group_filterButton.click();
		browser_wait(TestConstants.LONG_3000);

		// Scroll bottom of the page
		scrollDownPage(browser);
		browser_wait(TestConstants.LONG_4000);

		// Scroll Up of the page
		scrollUpPage(browser);
		browser_wait(TestConstants.LONG_2000);

		// Click_on_undoLast_button_to_undone
		WebElement manageUser_undoLast_Button = doc_get(
				"manageUser_undoLast_button_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_undoLast_Button_display_assert_false",
				manageUser_undoLast_Button.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		manageUser_undoLast_Button.click();
		browser_wait(TestConstants.LONG_3000);

		// Select_Username_radioButton_to_test
		WebElement manageUser_username_radioButton = doc_get(
				"manageUser_username_radioButton_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_username_radioButton_display_assert_false",
				manageUser_username_radioButton.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		manageUser_username_radioButton.click();
		browser_wait(TestConstants.LONG_2000);

		// Enter_any_value_in_textField_regarding_username_radioButton
		WebElement manageUser_contains_textfield = doc_get(
				"manageUser_contain_textField_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_contains_textfield_display_assert_false",
				manageUser_contains_textfield.isDisplayed());
		manageUser_contains_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("manageUser_username_contains_sendkeys"));
		browser_wait(TestConstants.LONG_1000);

		// Click_on_filterButton
		manageUser_group_filterButton = doc_get(
				"manageUser_refineFilter_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_group_filterButton.click();
		browser_wait(TestConstants.LONG_3000);

		// Scroll bottom of the page
		scrollDownPage(browser);
		browser_wait(TestConstants.LONG_4000);

		// Scroll Up of the page
		scrollUpPage(browser);
		browser_wait(TestConstants.LONG_2000);

		// Click_on_undoLast_button
		manageUser_undoLast_Button = doc_get(
				"manageUser_undoLast_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_undoLast_Button.click();
		browser_wait(TestConstants.LONG_3000);

		// Select_userEmail_radioButton_to_test
		WebElement manageUser_userEmail_radioButton = doc_get(
				"manageUser_userEmail_radioButton_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_userEmail_radioButton_display_assert_false",
				manageUser_userEmail_radioButton.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		manageUser_userEmail_radioButton.click();
		browser_wait(TestConstants.LONG_2000);

		// Enter_any_text_in_textField_regarding_userEmail_radioButton
		manageUser_contains_textfield = doc_get(
				"manageUser_contain_textField_xpath", browser);
		manageUser_contains_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("manageUser_userEmail_contains_sendkeys"));
		browser_wait(TestConstants.LONG_1000);

		// Click_on_filterButton
		manageUser_group_filterButton = doc_get(
				"manageUser_refineFilter_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_group_filterButton.click();
		browser_wait(TestConstants.LONG_3000);

		// Scroll bottom of the page
		scrollDownPage(browser);
		browser_wait(TestConstants.LONG_4000);

		// Scroll Up of the page
		scrollUpPage(browser);
		browser_wait(TestConstants.LONG_2000);

		// Click_on_undoLast_button
		manageUser_undoLast_Button = doc_get(
				"manageUser_undoLast_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_undoLast_Button.click();
		browser_wait(TestConstants.LONG_3000);

		// Select_deviceName_radioButton_to_test
		WebElement manageUser_deviceName_radioButton = doc_get(
				"manageUser_deviceName_radioButton_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_deviceName_radioButton_display_assert_false",
				manageUser_deviceName_radioButton.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		manageUser_deviceName_radioButton.click();
		browser_wait(TestConstants.LONG_2000);

		// Enter_text_in_textField_regarding_deviceName_radioButton
		manageUser_contains_textfield = doc_get(
				"manageUser_contain_textField_xpath", browser);
		manageUser_contains_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("manageUser_deviceName_contains_sendkeys"));
		browser_wait(TestConstants.LONG_1000);

		// Click_on_filterButton
		manageUser_group_filterButton = doc_get(
				"manageUser_refineFilter_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_group_filterButton.click();
		browser_wait(TestConstants.LONG_3000);

		// Scroll bottom of the page
		scrollDownPage(browser);
		browser_wait(TestConstants.LONG_4000);

		// Scroll Up of the page
		scrollUpPage(browser);
		browser_wait(TestConstants.LONG_2000);

		// Click_on_clearFilter_button_to_clear_all_filters
		WebElement manageUser_clearFilter_Button = doc_get(
				"manageUser_clearFilter_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_clearFilter_Button.click();
		browser_wait(TestConstants.LONG_3000);

		// Select_any_user_to_perform_operation
		List<WebElement> manageUser_updateOption_dropDownList_elements = doc_list_get(
				"manageUser_updateOption_dropDownList_elements_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_updateOption_dropDownList_elements_display_assert_false",
				manageUser_updateOption_dropDownList_elements.size() > 0);
		browser_wait(TestConstants.LONG_1000);
		(manageUser_updateOption_dropDownList_elements.get(0).findElement(By
				.tagName("input"))).click();
		browser_wait(TestConstants.LONG_3000);

		// Click_on_updateOption_dropDownList
		WebElement manageUser_updateOption_dropDownList = doc_get(
				"manageUser_updateOption_dropDownList_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_updateOption_dropDownList.click();
		browser_wait(TestConstants.LONG_1000);

		// Select_delete_operation_from_dropDownList
		WebElement manageUser_updateOption_dropDownList_delete = doc_get(
				"manageUser_updateOption_dropDownList_delete_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_updateOption_dropDownList_delete.click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_updateButton_to_update_selected_user
		WebElement manageUser_update_button = doc_get(
				"manageUser_updateOption_update_button", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_update_button.click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_deleteAll_button_to_delete_the_selected_user
		WebElement manageUser_deleteAll_button = doc_get(
				"manageUser_updateOption_deleteAll_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_deleteAll_button.click();
		browser_wait(TestConstants.LONG_3000);

		// Assert_for_deleted_user
		WebElement manageUser_show_message = doc_get(
				"manageUser_show_message_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_user_has_been_deleted_display_assert_false",
				manageUser_show_message.getText().contains(
						"The users have been deleted."));

		// Select_any_user_to_perform_addGroup_operation
		manageUser_updateOption_dropDownList_elements = doc_list_get(
				"manageUser_updateOption_dropDownList_elements_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		(manageUser_updateOption_dropDownList_elements.get(0).findElement(By
				.tagName("input"))).click();
		// (manageUser_updateOption_dropDownList_elements
		// .get(manageUser_updateOption_dropDownList_elements.size() - 3)
		// .findElement(By.tagName("input"))).click();
		browser_wait(TestConstants.LONG_3000);

		// Click_on_dropDownList
		manageUser_updateOption_dropDownList = doc_get(
				"manageUser_updateOption_dropDownList_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_updateOption_dropDownList.click();
		browser_wait(TestConstants.LONG_1000);

		// Select_any_group_from_addGroup_list_in_dropDownList
		WebElement manageUser_updateOption_dropDownList_addGroup = doc_get(
				"manageUser_updateOption_dropDownList_addGroup_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_updateOption_dropDownList_addGroup.click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_updateButton
		manageUser_update_button = doc_get(
				"manageUser_updateOption_update_button", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_update_button.click();
		browser_wait(TestConstants.LONG_3000);

		// Assert_for_add_a_new_group_in_an_user's_account
		manageUser_show_message = doc_get("manageUser_show_message_xpath",
				browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_group_has_been_added_display_assert_false",
				manageUser_show_message.getText().contains(
						"The update has been performed."));
		browser_wait(TestConstants.LONG_1000);

		// Select_any_user_to_perform_removeGroup_operation
		manageUser_updateOption_dropDownList_elements = doc_list_get(
				"manageUser_updateOption_dropDownList_elements_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		(manageUser_updateOption_dropDownList_elements.get(0).findElement(By
				.tagName("input"))).click();
		// (manageUser_updateOption_dropDownList_elements
		// .get(manageUser_updateOption_dropDownList_elements.size() - 3)
		// .findElement(By.tagName("input"))).click();
		browser_wait(TestConstants.LONG_3000);

		// Click_on_dropDownList
		manageUser_updateOption_dropDownList = doc_get(
				"manageUser_updateOption_dropDownList_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_updateOption_dropDownList.click();
		browser_wait(TestConstants.LONG_1000);

		// Select_any_group_from_removeGroup_in_dropDownList
		WebElement manageUser_updateOption_dropDownList_removeGroup = doc_get(
				"manageUser_updateOption_dropDownList_removeGroup_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_updateOption_dropDownList_removeGroup.click();
		browser_wait(TestConstants.LONG_2000);

		// Click_on_update_button
		manageUser_update_button = doc_get(
				"manageUser_updateOption_update_button", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_update_button.click();
		browser_wait(TestConstants.LONG_3000);

		// Assert_for_removed_group_from_selected_user's_account
		manageUser_show_message = doc_get("manageUser_show_message_xpath",
				browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_group_has_been_removed_display_assert_false",
				manageUser_show_message.getText().contains(
						"The update has been performed."));
		browser_wait(TestConstants.LONG_1000);

		// Select_any_user_to_perform_deprovision_operation
		manageUser_updateOption_dropDownList_elements = doc_list_get(
				"manageUser_updateOption_dropDownList_elements_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		(manageUser_updateOption_dropDownList_elements.get(0).findElement(By
				.tagName("input"))).click();
		browser_wait(TestConstants.LONG_3000);

		// Click_on_update_button
		manageUser_update_button = doc_get(
				"manageUser_updateOption_update_button", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_update_button.click();
		browser_wait(TestConstants.LONG_3000);

		// Assert_for_deprovision_a_user
		manageUser_show_message = doc_get("manageUser_show_message_xpath",
				browser);
		Assert.assertTrue(
				"bizappcenter_manageUser_deprovision_user_display_assert_false",
				manageUser_show_message.getText().contains(
						"The update has been performed."));
		browser_wait(TestConstants.LONG_2000);

		// to check that the selected user deprovision or not
		// bizapp_click_on_Manage_Tab
		WebElement bizapp_manage_tab = doc_get("bizappcenter_manage_tab_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_manage_tab.click();
		browser_wait(TestConstants.LONG_1000);

		// bizapp_manage_click_on_appDistribution_Tab
		WebElement bizapp_appDistribution_tab = doc_get(
				"bizappcenter_manage_appDistribution_tab_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_appDistribution_tab.click();
		browser_wait(TestConstants.LONG_3000);

		// click on deprovision list to see deprovision user
		WebElement deprovision_List = doc_get(
				"appDistribution_tab_deprovision_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		deprovision_List.click();
		browser_wait(TestConstants.LONG_3000);

		WebElement deprovision_User = doc_get(
				"appDistribution_tab_deprovision_UserList_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		deprovision_User.click();
		browser_wait(TestConstants.LONG_4000);

		bizapp_user_tab = doc_get("bizappcenter_user_tab_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_user_tab.click();
		browser_wait(TestConstants.LONG_1000);

		// bizapp_UserTab_click_on_manageUser_Tab
		bizapp_manageUser_tab = doc_get(
				"bizappcenter_user_manageUserTab_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_manageUser_tab.click();
		browser_wait(TestConstants.LONG_3000);

		// Click_on_Edit_link_to_edit_a_user's_account
		WebElement manageUser_updateOption_tableList_edit = doc_get(
				"manageUser_updateOption_tableList_edit_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_updateOption_tableList_edit.click();
		browser_wait(TestConstants.LONG_2000);

		// Select_active_status_radioButton_to_change_status_of_deprovision_user
		WebElement manageUser_updateOption_edit_status_active_radioButton = doc_get(
				"manageUser_updateOption_edit_status_active_radioButton_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_updateOption_edit_status_active_radioButton.click();
		browser_wait(TestConstants.LONG_1000);

		// Click_on_saveButton_to_save_changes
		WebElement manageUser_updateOption_edit_saveButton = doc_get(
				"manageUser_updateOption_edit_saveButton_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		manageUser_updateOption_edit_saveButton.click();
		browser_wait(TestConstants.LONG_3000);

		// Click_on_User_tab
		bizapp_user_tab = doc_get("bizappcenter_user_tab_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_user_tab.click();
		browser_wait(TestConstants.LONG_1000);

		// bizapp_User_click_on_manageUse_Tab
		bizapp_manageUser_tab = doc_get(
				"bizappcenter_user_manageUserTab_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_user_manageUser_tab_display_assert_false",
				bizapp_manageUser_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizapp_manageUser_tab.click();
		browser_wait(TestConstants.LONG_3000);

		// log out from bizappcenter
		WebElement logOut_bizappCenter = doc_get(
				"bizAppCenter_logOut_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		logOut_bizappCenter.click();
		browser_wait(TestConstants.LONG_3000);
	}
}
