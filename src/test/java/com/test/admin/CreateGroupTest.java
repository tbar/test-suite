package com.test.admin;

import java.util.List;
import org.openqa.selenium.WebElement;
import junit.framework.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.test.util.SeleniumUtil;
import com.test.util.TestConstants;

public class CreateGroupTest extends SeleniumUtil {
	private final String addGroupTest_PROPERTIES_HTML_ID_FILENAME = "AddGroup_HTML_ID.properties";
	private final String addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME = "AddGroup_Assert_Values.properties";

	@BeforeMethod
	protected void bizappcenterTest_suite_pre_function() throws Exception {
		bizappcenterTest_properties_Assert_Values_file = addGroupTest_PROPERTIES_ASSERT_VALUES_FILENAME;
		bizappcenterTest_properties_HTML_ID_file = addGroupTest_PROPERTIES_HTML_ID_FILENAME;
		bizappcenterTest_suite_setup();
	}

	// Test_case for Addgroup in bizappcenter
	@Test(priority = TestConstants.INT_1)
	public void bizappcenter_addGroup_test() throws Exception {
		// Login_for_Test_User
		bizappLogin(browser,"AdminClassTest_Assert_Values.properties","Bizapp_Login_HTML_ID1.properties");

		// Click_on_Users_Tab
		WebElement bizapp_user_tab = doc_get("bizappcenter_user_tab_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		bizapp_user_tab.click();
		browser_wait(TestConstants.LONG_2000);

		// bizapp_UsersTab_Click_on_AddGroup_Tab
		WebElement bizapp_addGroup_tab = doc_get("bizappcenter_addGroup_xpath",
				browser);
		Assert.assertTrue(
				"bizappcenter_user_addGroup_tab_display_assert_false",
				bizapp_addGroup_tab.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		bizapp_addGroup_tab.click();
		browser_wait(TestConstants.LONG_3000);

		// Enter Invalid Test_group_name
		WebElement addGroup_createGroup_textfield = doc_get(
				"addGroup_create_group_textfield_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addGroup_createGroup_textfield_display_assert_false",
				addGroup_createGroup_textfield.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addGroup_createGroup_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_wrong_addGroup_createUser"));
		browser_wait(TestConstants.LONG_2000);

		// Select parent of given Invalid_Test_group_name
		List<WebElement> addGroup_select_parent_textarea = doc_list_get(
				"addGroup_select_parent_textarea_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addGroup_select_parent_textarea_display_assert_false",
				addGroup_select_parent_textarea.size() > 0);
		addGroup_select_parent_textarea.get(2).click();
		browser_wait(TestConstants.LONG_2000);

		// Click on AddGroup_button
		WebElement addGroup_button_click = doc_get(
				"addGroup_button_addgroup_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addGroup_button_click_display_assert_false",
				addGroup_button_click.isDisplayed());
		browser_wait(TestConstants.LONG_1000);
		addGroup_button_click.click();

		// Assert for an Invalid GroupName
		browser_wait(TestConstants.LONG_2000);
		WebElement addGroup_error_message = doc_get(
				"addGroup_error_message_xpath", browser);
		Assert.assertTrue(
				"bizappcenter_addGroup_Invalid_groupName_error_message_display_assert_false",
				addGroup_error_message.getText().contains(
						"Group name must not contain invalid characters"));

		// Enter Existing Test_group_name
		addGroup_createGroup_textfield = doc_get(
				"addGroup_create_group_textfield_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addGroup_createGroup_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addGroup_createGroup_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addGroup_createUser"));
		browser_wait(TestConstants.LONG_2000);

		// Select Parent_group of given GroupName
		addGroup_select_parent_textarea = doc_list_get(
				"addGroup_select_parent_textarea_xpath", browser);
		addGroup_select_parent_textarea.get(2).click();
		browser_wait(TestConstants.LONG_2000);

		addGroup_button_click = doc_get("addGroup_button_addgroup_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		addGroup_button_click.click();
		browser_wait(TestConstants.LONG_2000);

		// Assert for an Existing GroupName
		addGroup_error_message = doc_get("addGroup_error_message_xpath",
				browser);
		Assert.assertTrue(
				"bizappcenter_addGroup_Existing_groupName_error_message_display_assert_false",
				addGroup_error_message
						.getText()
						.contains(
								"The Group name testing level1 already exists. Please choose another role name."));

		// Test_case for Group name exceeding the max allowed characters (60)
		// Enter max length group length
		addGroup_createGroup_textfield = doc_get(
				"addGroup_create_group_textfield_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addGroup_createGroup_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addGroup_createGroup_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addGroup_maxCharacter_groupName"));
		browser_wait(TestConstants.LONG_2000);

		// String fixLengthOfGivenString =
		// bizappcenterTest_properties_assert_values_get(
		// "bizappcenter_addGroup_maxCharacter_groupName")
		// .substring(0, 59);

		// Select Parent of given Test_group_name
		addGroup_select_parent_textarea = doc_list_get(
				"addGroup_select_parent_textarea_xpath", browser);
		addGroup_select_parent_textarea.get(2).click();
		browser_wait(TestConstants.LONG_2000);

		// Click on AddGroup Button
		addGroup_button_click = doc_get("addGroup_button_addgroup_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		addGroup_button_click.click();
		browser_wait(TestConstants.LONG_2000);

		addGroup_select_parent_textarea = doc_list_get(
				"addGroup_select_parent_textarea_xpath", browser);
		for (int i = 0; i < addGroup_select_parent_textarea.size(); i++) {
			// if((addGroup_select_parent_textarea.get(i).getText()).equals(fixLengthOfGivenString))
			// {
			// }

			if (((addGroup_select_parent_textarea.get(i).getText()).replace(
					'-', ' ').trim()).length() > 60) {
				Assert.assertTrue(
						"bizappcenter_group_has_been_added_error_message_display_assert_false",
						(addGroup_select_parent_textarea.get(i).getText())
								.length() <= 60);
			}
		}

		// Assert for a new Group, has been added or not
		addGroup_error_message = doc_get("addGroup_error_message_xpath",
				browser);
		Assert.assertTrue(
				"bizappcenter_group_has_been_added_error_message_display_assert_false",
				addGroup_error_message.getText().contains(
						"The Group has been added."));

		// Test_case for Add_a_group_at_the_top_level and Add a group
		// to multiple Parents in bizappcenter
		// Enter GroupName
		addGroup_createGroup_textfield = doc_get(
				"addGroup_create_group_textfield_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addGroup_createGroup_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addGroup_createGroup_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addGroup_createUser2"));
		browser_wait(TestConstants.LONG_2000);

		// Select Parent of given Test_group_name
		addGroup_select_parent_textarea = doc_list_get(
				"addGroup_select_parent_textarea_xpath", browser);
		addGroup_select_parent_textarea.get(2).click();
		browser_wait(TestConstants.LONG_2000);
		addGroup_select_parent_textarea.get(1).click();
		browser_wait(TestConstants.LONG_2000);
		addGroup_select_parent_textarea.get(0).click();
		browser_wait(TestConstants.LONG_2000);

		int countForMultipleSelection = 0;
		for (int i = 0; i < addGroup_select_parent_textarea.size(); i++) {
			if (addGroup_select_parent_textarea.get(i).isSelected()) {
				countForMultipleSelection++;
			}
		}

		// Assert for Add a group to multiple Parents
		Assert.assertTrue(
				"bizappcenter_group_selected_multiple_parent_display_assert_false",
				countForMultipleSelection == 1);

		// Click on AddGroup Button
		addGroup_button_click = doc_get("addGroup_button_addgroup_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		addGroup_button_click.click();
		browser_wait(TestConstants.LONG_2000);

		// Assert for Add a new Group at the top level
		addGroup_error_message = doc_get("addGroup_error_message_xpath",
				browser);
		Assert.assertTrue(
				"bizappcenter_group_added_at_a_top_level_error_message_display_assert_false",
				addGroup_error_message.getText().contains(
						"The Group has been added."));

		// Add groups until the max allowed level
		// Enter Groups of different Levels
		addGroup_createGroup_textfield = doc_get(
				"addGroup_create_group_textfield_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		addGroup_createGroup_textfield.clear();
		browser_wait(TestConstants.LONG_1000);
		addGroup_createGroup_textfield
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addGroup_testMaxLevel"));
		browser_wait(TestConstants.LONG_2000);

		addGroup_select_parent_textarea = doc_list_get(
				"addGroup_select_parent_textarea_xpath", browser);
		addGroup_select_parent_textarea.get(0).click();
		browser_wait(TestConstants.LONG_2000);

		addGroup_button_click = doc_get("addGroup_button_addgroup_xpath",
				browser);
		browser_wait(TestConstants.LONG_1000);
		addGroup_button_click.click();
		browser_wait(TestConstants.LONG_3000);
		addGroup_select_parent_textarea = doc_list_get(
				"addGroup_select_parent_textarea_xpath", browser);

		int positionOfGroupLevel = 0;
		for (int i = 0; i < addGroup_select_parent_textarea.size(); i++) {
			if (((addGroup_select_parent_textarea.get(i).getText()).replace(
					'-', ' ').trim())
					.equals(bizappcenterTest_properties_assert_values_get("bizappcenter_addGroup_testMaxLevel"))) {
				positionOfGroupLevel = i;
				break;
			}

		}

		for (int i = 1; i < 10; i++) {
			addGroup_createGroup_textfield = doc_get(
					"addGroup_create_group_textfield_xpath", browser);
			browser_wait(TestConstants.LONG_1000);
			addGroup_createGroup_textfield.clear();
			browser_wait(TestConstants.LONG_1000);
			addGroup_createGroup_textfield
					.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_addGroup_testMaxLevel")
							+ "-" + i);
			browser_wait(TestConstants.LONG_2000);

			addGroup_select_parent_textarea = doc_list_get(
					"addGroup_select_parent_textarea_xpath", browser);
			addGroup_select_parent_textarea.get((positionOfGroupLevel - 1) + i)
					.click();
			browser_wait(TestConstants.LONG_2000);

			addGroup_button_click = doc_get("addGroup_button_addgroup_xpath",
					browser);
			browser_wait(TestConstants.LONG_1000);
			addGroup_button_click.click();
			browser_wait(TestConstants.LONG_3000);

		}

	}

}
