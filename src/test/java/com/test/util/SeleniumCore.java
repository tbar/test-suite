package com.test.util;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class which has all required method for Selenium Test framework
 * 
 * @author Krishna Dhangar
 * 
 */
public class SeleniumCore {
	private static final String SAFARI = "Safari";
	private static final String FIREFOX = "Firefox";
	private static final String CHROME = "Chrome";
	private static final String IE = "IE";
	private static final String REMOTEWEBDRIVER_URL = "http://127.0.0.1:9515";

	// properties file used to launch the application
	protected static final String BIZAPPCENTERTEST_PROPERTIES_ENVIORNMENT_FILENAME = "selenium.properties";
	protected static final String BIZAPPCENTERTEST_ENVIORNMENT_FOLDERNAME = "environment";

	// To read the application properties
	private static Properties BIZAPPCENTERTEST_PROPERTIES;
	public static final String BIZAPPCENTER_LOGIN_URL;
	public static final String FILE_NAME;
	public static final String BROWSER;
	public static final Long WAIT_TIME;
	public static final Long WAIT_TIME_SHORT;

	private static Logger logger = LoggerFactory.getLogger(SeleniumCore.class);
	// static block to initialize application properties
	static {
		InputStream instream = SeleniumCore.class
				.getClassLoader()
				.getResourceAsStream(
						BIZAPPCENTERTEST_ENVIORNMENT_FOLDERNAME
								+ File.separator
								+ BIZAPPCENTERTEST_PROPERTIES_ENVIORNMENT_FILENAME);
		BIZAPPCENTERTEST_PROPERTIES = new Properties();
		try {
			BIZAPPCENTERTEST_PROPERTIES.load(instream);
		} catch (Exception e) {
			// CODEREAD SUPPRESS: ON
			logger.debug("Exception in loading environment property file "
					+ BIZAPPCENTERTEST_PROPERTIES_ENVIORNMENT_FILENAME,
					e.getCause());
			// CODEREAD SUPPRESS: OFF
		}
		BIZAPPCENTER_LOGIN_URL = getSpecificProperty(TestConstants.BIZAPPCENTER_LOGIN_URL);
		BROWSER = getSpecificProperty(TestConstants.BROWSER);
		FILE_NAME = getSpecificProperty(TestConstants.BIZAPPCENTERTEST_FOLDERNAME_PC);
		if (null != BROWSER && IE.equalsIgnoreCase(BROWSER)) {
			WAIT_TIME = TestConstants.LONG_10000;
			WAIT_TIME_SHORT = TestConstants.LONG_3000;
		} else {
			WAIT_TIME = TestConstants.LONG_7000;
			WAIT_TIME_SHORT = TestConstants.LONG_2000;
		}
	}

	/**
	 * Provides getSpecific property
	 * 
	 * @return String
	 * @param property
	 *            property param
	 */
	private static String getSpecificProperty(String property) {
		return (null != BIZAPPCENTERTEST_PROPERTIES.getProperty(property)) ? BIZAPPCENTERTEST_PROPERTIES
				.getProperty(property) : "";
	}

	/**
	 * Provides WebDriver as per specified in Selenium.properties file.
	 * 
	 * @throws MalformedURLException
	 *             to catch MalformedURLException
	 * @return WebDriver
	 */
	public static WebDriver getDriver() throws MalformedURLException {
		String runBrowser = BROWSER;
		WebDriver driver = null;
		DesiredCapabilities capabilities = null;
		ChromeOptions options = null;
		if (FIREFOX.equalsIgnoreCase(runBrowser)) {
			driver = new FirefoxDriver();
		} else if (CHROME.equalsIgnoreCase(runBrowser)) {
			try {
				capabilities = DesiredCapabilities.chrome();
				options = new ChromeOptions();
			} catch (Exception e) {
				logger.debug("SeleniumCore ChromeOption Exception : ",
						e.getCause());
			}
			 options.addArguments(Arrays.asList("--test-type",
			 "--start-maximized","--enable-precache=true"));
//			options.addArguments(Arrays.asList("-incognito",
//					"--start-maximized"));
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			driver = new RemoteWebDriver(new URL(REMOTEWEBDRIVER_URL),
					capabilities);
		} else if (IE.equalsIgnoreCase(runBrowser)) {
			driver = new RemoteWebDriver(new URL(REMOTEWEBDRIVER_URL),
					DesiredCapabilities.internetExplorer());
		} else if (SAFARI.equalsIgnoreCase(runBrowser)) {
			if (isSupportedPlatform()) {
				driver = new SafariDriver();
			} else {
				driver = new FirefoxDriver();
			}
		} else {

			driver = new HtmlUnitDriver();
		}
		return driver;
	}

	/**
	 * Verifies platform of test run.
	 * 
	 * @return boolean
	 */
	private static boolean isSupportedPlatform() {
		Platform current = Platform.getCurrent();
		return Platform.MAC.is(current) || Platform.WINDOWS.is(current);
	}
}
