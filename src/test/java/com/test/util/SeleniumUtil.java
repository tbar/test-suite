package com.test.util;

import static com.test.util.SeleniumCore.BIZAPPCENTER_LOGIN_URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;

import com.test.util.SeleniumCore;
import com.test.util.TestConstants;

/**
 * This class which has all required utility method for test cases.
 * 
 */
public class SeleniumUtil {
	private static Logger logger = LoggerFactory.getLogger(SeleniumUtil.class);
	protected static WebDriver browser;
	private Properties bizappcenterTest_properties_assert_values = null;
	private Properties bizappcenterTest_properties_html_id = null;
	protected String bizappcenterTest_properties_Assert_Values_file = null;
	protected String bizappcenterTest_properties_HTML_ID_file = null;

	private void bizappcenterTest_suite_properties_initialize()
			throws MalformedURLException, IOException, InterruptedException {
		browser = SeleniumCore.getDriver();
		logger.error("logger starting!!!!!!!!! ");
		// Get all tabs in a list which are currently opened in windows
		browser.get(BIZAPPCENTER_LOGIN_URL);
		bizappcenterTest_properties_assert_values = new Properties();
		bizappcenterTest_properties_html_id = new Properties();
		try {
			bizappcenterTest_properties_Assert_set(bizappcenterTest_properties_Assert_Values_file);
			bizappcenterTest_properties_HTML_ID_set(bizappcenterTest_properties_HTML_ID_file);
		} catch (Exception e) {
			logger.debug("SeleniumUtil set Assert & HTML_ID Exception : ",
					e.getCause());
		}
	}

	public void bizappcenterTest_suite_setup() throws MalformedURLException,
			IOException, InterruptedException {
		bizappcenterTest_suite_properties_initialize();
	}

	/**
	 * Method to load the property file for this page
	 * 
	 * @param fileName
	 *            param
	 * @throws IOException
	 *             to catch IOException
	 */
	public void bizappcenterTest_properties_Assert_set(String fileName)
			throws IOException {
		if (fileName != null) {
			if (bizappcenterTest_properties_assert_values == null) {
				bizappcenterTest_properties_assert_values = new Properties();
			}
			InputStream instream = SeleniumUtil.class.getClassLoader()
					.getResourceAsStream(
							TestConstants.PROPERTIES + File.separator
									+ fileName);
			bizappcenterTest_properties_assert_values.load(instream);
		}
	}

	/**
	 * Set HTML Id properties file
	 * 
	 * @return
	 * @param fileName
	 *            param
	 * @throws IOException
	 *             to catch IOException
	 */
	public void bizappcenterTest_properties_HTML_ID_set(String fileName)
			throws IOException {
		if (fileName != null) {
			if (bizappcenterTest_properties_html_id == null) {
				bizappcenterTest_properties_html_id = new Properties();
			}
			InputStream instream = SeleniumUtil.class.getClassLoader()
					.getResourceAsStream(
							TestConstants.PROPERTIES + File.separator
									+ fileName);
			bizappcenterTest_properties_html_id.load(instream);
		}
	}

	/**
	 * Sleep thread for given waitingTime
	 * 
	 * @param waitingTime
	 *            param
	 * @throws InterruptedException
	 *             to catch InterruptedException
	 */
	public static void browser_wait(Long waitingTime)
			throws InterruptedException {
		Thread.sleep(waitingTime);
	}

	/**
	 * Returns value to assert if the related property file is loaded
	 * 
	 * @param key
	 *            key
	 * @return String
	 */
	public String bizappcenterTest_properties_assert_values_get(String key) {
		String value = null;
		if (bizappcenterTest_properties_assert_values.size() > 0) {
			value = bizappcenterTest_properties_assert_values.getProperty(key);
		}
		return value;
	}
	
	/**
	 * Scroll down of the page
	 */
	public void scrollDownPage(WebDriver browser) throws Exception{
		Actions actions = new Actions(browser);
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
	}
	
	/**
	 * Scroll up of the page
	 */
	public void scrollUpPage(WebDriver browser) throws Exception{
		Actions actions = new Actions(browser);
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).perform();
	}
	
	/**
	 * Method return property value for given id
	 * 
	 * @param propId
	 *            propId
	 * @return String string
	 */
	public String bizappcenterTest_properties_HTML_ID_get(String propId) {
		String value = null;
		if (bizappcenterTest_properties_html_id.size() > 0) {
			value = bizappcenterTest_properties_html_id.getProperty(propId);
		}
		return value;
	}

	/**
	 * This method return the web-element of given xpath
	 */
	public WebElement doc_get(String xpath, WebDriver browser) throws Exception {
		WebElement elementPath = null;
		elementPath = browser.findElement(By
				.xpath(bizappcenterTest_properties_HTML_ID_get(xpath)));
		return elementPath;
	}

	/**
	 * This method return the list of web-elements of given xpath
	 */
	public List<WebElement> doc_list_get(String xpath, WebDriver browser)
			throws Exception {
		List<WebElement> listElementPath = null;
		listElementPath = browser.findElements(By
				.xpath(bizappcenterTest_properties_HTML_ID_get(xpath)));
		return listElementPath;
	}

	/**
	 * drag and drop
	 * 
	 * @param dragElement
	 *            param
	 * @param dropElement
	 *            paramdrop
	 */
	public void dragAndDrop(WebElement dragElement, WebElement dropElement) {
		Actions builder = new Actions(browser);
		Action dragAndDrop = builder.clickAndHold(dragElement)
				.moveToElement(dropElement).release(dropElement).build();
		dragAndDrop.perform();
	}
	 
	/**
	 * Move and Release
	 * 
	 * @param moveElementForward
	 *            param
	 * @param releaseElement
	 *            paramdrop
	 */
	public void MoveElementForward(WebElement dragElement,
			WebElement dropElement) {
		Actions builder = new Actions(browser);
		Action dragAndDrop = builder
				.clickAndHold(dragElement)
				.moveByOffset(dragElement.getLocation().getX() + 10,
						dragElement.getLocation().getY() + 0)
				.release(dropElement).build();
		dragAndDrop.perform();
	}

	/**
	 * Move and Release
	 * 
	 * @param moveElementBackward
	 *            param
	 * @param releaseElement
	 *            paramdrop
	 */
	public void MoveElementBackward(WebElement dragElement,
			WebElement dropElement) {
		Actions builder = new Actions(browser);
		Action dragAndDrop = builder
				.clickAndHold(dragElement)
				.moveByOffset((dragElement.getLocation().getX()) - 25,
						dragElement.getLocation().getY() - 0)
				.release(dropElement).build();
		dragAndDrop.perform();
	}

	/**
	 * Captures Screen shots
	 * 
	 * @param paramBrowser
	 *            browser param
	 * @param fileName
	 *            file
	 * @throws IOException
	 *             to catch IOException
	 */
	public static void logScreen(WebDriver paramBrowser, String fileName)
			throws IOException {
		File scrFile = ((TakesScreenshot) paramBrowser)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(fileName));
	}

	/**
	 * Handle a particular window
	 * 
	 * @param paramBrowser
	 *            browser param
	 * @param title
	 *            param
	 * @throws InterruptedException
	 *             to catch InterruptedException
	 * @return String
	 */
	public static WebDriver gotoWindow(WebDriver paramBrowser, String title)
			throws InterruptedException {
		WebDriver ws = null;
		Set<String> winHandles = paramBrowser.getWindowHandles();
		Iterator<String> ite = winHandles.iterator();
		if (winHandles.size() == 2) {

			while (ite.hasNext()) {

				if (paramBrowser.getTitle().equals(title)) {
					paramBrowser.close();
					paramBrowser = paramBrowser.switchTo().window(
							ite.next().toString());
					break;
				} else {
					ws = paramBrowser;
					paramBrowser.switchTo().window(ite.next().toString())
							.close();
					return ws;
				}
			}
		}
		return paramBrowser;
	}

	/**
	 * This function get all the tabs and can switch to next tab of current
	 * opening tab
	 */
	public static void browser_tabs_switch(WebDriver browser)
			throws InterruptedException {
		List<String> tabLists = new ArrayList<String>(
				browser.getWindowHandles());
		// change focus to new tab
		if (tabLists.size() >= TestConstants.INT_2) {
			browser.switchTo().window(tabLists.get(1));
			browser.close();
			browser.switchTo().window(tabLists.get(0));
		}
	}

	public static List<String> getListOfOpenedTabs()
			throws InterruptedException {
		List<String> tabLists = new ArrayList<String>(
				browser.getWindowHandles());
		return tabLists;
	}

	/**
	 * To switch between popups and different windows
	 * 
	 * @param paramBrowser
	 *            browser param
	 * @param parentWindowHandle
	 *            param
	 */
	public static void goToParentWindow(WebDriver paramBrowser,
			String parentWindowHandle) {
		paramBrowser.switchTo().window(parentWindowHandle);
	}

	/**
	 * To close a window with a known title
	 * 
	 * @param paramBrowser
	 *            browser param
	 * @param title
	 *            title param
	 * @return boolean
	 */
	public static boolean closeWindow(WebDriver paramBrowser, String title) {
		if (paramBrowser.getTitle().equals(title)) {
			paramBrowser.close();
			return true;
		}
		return false;
	}

	/**
	 * drag and drop
	 * 
	 * @param dragElement
	 *            param
	 * @param dropElement
	 *            paramdrop
	 */
	public void browser_mouseOver(WebElement dragElement, WebDriver browser)
			throws InterruptedException {
		browser_wait(TestConstants.LONG_2000);
		Actions builder = new Actions(browser);
		builder.moveToElement(dragElement).perform();
		browser_wait(TestConstants.LONG_2000);
	}

	/**
	 * get unique id
	 * 
	 * @return long
	 */
	public static long timestamp_unique_get() {
		return new Date().getTime();
	}

	/**
	 * Bizappcenter AdminLogin Method
	 */
	public void bizappLogin(WebDriver browser, String assertFile, String htmlFile) throws Exception {

		try {
			bizappcenterTest_properties_Assert_Values_file = assertFile;
			bizappcenterTest_properties_HTML_ID_file = htmlFile;
			try {
				bizappcenterTest_properties_Assert_set(bizappcenterTest_properties_Assert_Values_file);
				bizappcenterTest_properties_HTML_ID_set(bizappcenterTest_properties_HTML_ID_file);
			} catch (Exception e) {
				logger.debug("SeleniumUtil set Assert & HTML_ID Exception : ",
						e.getCause());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		browser_wait(TestConstants.LONG_2000);
		// bizappcenter_login_username
		WebElement bizappcenter_username = doc_get(
				"bizappcenter_username_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_username
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_login_username_sendkeys"));

		// bizappcenter_login_password
		browser_wait(TestConstants.LONG_2000);
		WebElement bizappcenter_password = doc_get(
				"bizappcenter_password_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_password
				.sendKeys(bizappcenterTest_properties_assert_values_get("bizappcenter_login_password_sendkeys"));

		// bizappcenter_login_button_click
		browser_wait(TestConstants.LONG_2000);
		WebElement bizappcenter_login_button = doc_get(
				"bizappcenter_login_button_xpath", browser);
		browser_wait(TestConstants.LONG_1000);
		bizappcenter_login_button.click();
		browser_wait(TestConstants.LONG_7000);
	}

	/**
	 * TearDown method to logout to the app and close browser
	 * 
	 * @throws InterruptedException
	 *             to catch InterruptedException
	 */

	@AfterMethod
	public void tearDown() throws InterruptedException {
		browser_wait(TestConstants.LONG_2000);
		browser.quit();
	}

	/**
	 * This method will run after all tests run open the index.html file which
	 * exist in target/sur after running all tests
	 * 
	 * @throws InterruptedException
	 * @throws MalformedURLException
	 */
	@AfterSuite
	public void report() throws InterruptedException, MalformedURLException {
		SeleniumCore.getDriver().get(
				"file:" + System.getProperty(TestConstants.USER_DIR)
						+ File.separator + TestConstants.SURFLINE_REPORT
						+ File.separator + TestConstants.HTML
						+ File.separator + TestConstants.INDEX);
	}
}
