package com.test.util;

/**
 * This class which has all required constants for test cases.
 * 
 */
public class TestConstants {
	public static final Long LONG_1000 = 1000L;
	public static final Long LONG_3000 = 3000L;
	public static final Long LONG_2000 = 2000L;
	public static final Long LONG_5000 = 5000L;
	public static final Long LONG_4000 = 4000L;
	public static final Long LONG_7000 = 7000L;
	public static final Long LONG_10000 = 10000L;
	public static final Long LONG_30000 = 30000L;
	public static final int INT_30 = 30;
	public static final int INT_0 = 0;
	public static final int INT_1 = 1;
	public static final int INT_2 = 2;
	public static final int INT_3 = 3;
	public static final int INT_4 = 4;
	public static final int INT_5 = 5;
	public static final int INT_6 = 6;
	public static final int INT_7 = 7;
	public static final int INT_8 = 8;
	public static final int INT_9 = 9;
	public static final int INT_10 = 10;
	public static final int INT_11 = 11;
	public static final String BIZAPPCENTER_LOGIN_URL = "BIZAPPCENTER_LOGIN_URL";
	public static final String BROWSER = "BROWSER";
	public static final String BIZAPPCENTERTEST_FOLDERNAME_PC = "BIZAPPCENTERTEST_FOLDERNAME_PC";
	public static final String USER_DIR = "user.dir";
	public static final String PROPERTIES = "properties";
	public static final String WINDOWS = "Windows";
	public static final String TARGET = "target";
	public static final String SURFLINE_REPORT = "surefire-reports";
	public static final String HTML = "html";
	public static final String INDEX = "index.html";
}
